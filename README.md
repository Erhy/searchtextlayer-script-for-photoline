# SearchTextLayer Script for PhotoLine

 script for PhotoLine users on Microsoft Windows
 Amongst others with PhotoLine it is possible to edit PDF files.
 Opening a PDF file with Photoline, you see that the PDF file consists of many layers.
 
 To edit text in PDF files with Find/Replace in Edit Menu
 it is necessary to select the layer which contains the text.

 This script helps to select the layer which contains the text wanted.