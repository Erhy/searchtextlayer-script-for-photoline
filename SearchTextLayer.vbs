Option Explicit
'SearchTextLayer.vbs
' script for PhotoLine users on Microsoft Windows
' Amongst others with PhotoLine it is possible to edit PDF files.
' Opening a PDF file with Photoline, you see that the PDF file consists of many layers.
' To edit text in PDF files with Find/Replace in Edit Menu
' it is necessary to select the layer which contains the text.
' This script helps to select the layer which contains the text wanted.
' 
' When you locate this script file in the folder "C:\Program Files\PhotoLine\Defaults\Automation"
'  you are able to select and start the script in filter menu -> scripts
'
' Otherwise you may run the script from the command line
'  e.g.
'	cscript SearchTextLayer.vbs
'   in order to run the PhotoLine application with the file open.

' remarks:
'  When the script runs via PhotoLines filter menu -> scripts
'   WScript.Quit provokes an error message. 
'    Here further executions are prevented with >if constructs<

Dim pl
Dim doc
Dim page
Dim Response
Dim searchStr
Dim pageNr
Dim pageNow
Dim toQuit

toQuit = ""
Set pl = CreateObject("PhotoLine.Application")
Set doc = pl.ActiveDocument

pl.ArrayStartIndex = 0
pl.Visible = True

If doc Is Nothing Then
	MsgBox "No active PhotoLine Document!", vbOKOnly + vbError, "No PhotoLine Document" 
	'WScript.Quit(2)
	toQuit = "No active PhotoLine Document!"
End If

If Len(toQuit) = 0 Then
	searchStr = ""
	Do While Len(searchStr) < 1
		searchStr = InputBox("Enter a string, to search for","SearchString")
	Loop

	pageNr = 0
	For Each page In doc
		Set pageNow = page
		pageNr = pageNr + 1
		If Len(toQuit) = 0 Then
			If False Then 'change to True to be informed
				If pageNr > 1 Then
					Response = MsgBox( "Try next Page no. " + Cstr(pageNr), vbOK + vbCancel, "Try next Page" )
					If Response = vbCancel then
						MsgBox "nothing found on page " + Cstr(pageNr - 1), vbOKOnly, "Nothing found - Script terminates"
						'WScript.Quit(7)
						toQuit = "nothing found on page " + Cstr(pageNr - 1)
					End If
				End If
			End If
			If Len(toQuit) = 0 Then
				doc.ActivePage = page
				SearchInChilds(page.RootLayer)
			End If
		End If
	Next
	
	If Len(toQuit) = 0 Then			
		MsgBox "nothing found", vbOKOnly, "Nothing found - Script terminates"
		'WScript.Quit(8)
		toQuit = "nothing found"
	End If
End If

Sub SearchInChilds(layer)
	Dim child
	Dim FoundInL
	FoundInL = SearchInLayer(layer)
	if Len(FoundInL) > 0 Then
		MsgBox FoundInL, vbOKOnly, "here found - Script terminates"
		VisibleToAndParents( layer )
		
		pageNow.ActiveLayer = layer 'important
		toQuit = "here found - Script terminates"
		'WScript.Quit(0)
	End If
	
	'MsgBox "Try next Layer", vbOKOnly, "Try next Layer" 
	for each child in layer
		If Len(toQuit) = 0 Then
			SearchInChilds(child) ' rekusiv!
		End If
	Next
End Sub

Sub VisibleToAndParents(layer)
	Dim parentL
	layer.Visible = True
	Set parentL = layer.Parent
	If parentL Is Nothing Then
		'MsgBox "no Parent Layer!", vbOKOnly, "no Parent Layer!"
	Else
		'MsgBox "Parent Layer!", vbOKOnly, "Parent Layer!"
		VisibleToAndParents( parentL ) ' rekusiv!
	End If
End Sub

Function SearchInLayer(layer)
	SearchInLayer = ""
	' Is it a text layer? 4: PhotoLine.LayerType.LTText
	if (layer.Type = 4) Then
		Dim textRange
		Dim	endIndex
		Dim	actIndex
		Dim str
		Dim strPos
		
		' iterate over all attributes inside the text layer
		' textRange(0): start index of text, textRange(1): number of characters
		textRange = layer.TextRange

		actIndex = textRange(0)
		endIndex = actIndex + textRange(1)
		Do While (actIndex < endIndex)
			str = layer.Text(actIndex)
			If Len(str) > 0 Then
				'MsgBox str, vbOKOnly, "search for " + searchStr
				strPos = InStr(1, str, searchStr, VBTextCompare) 'vbTextCompare
				If strPos > 0 Then
					Response = MsgBox(str, _
						vbYesNo + vbDefaultButton1 + vbQuestion, _
					"If found press Yes" )
					If Response = vbYes Then
					'	'Found
						SearchInLayer = str
						Exit Do
					End If
				End If
			End If

			actIndex = actIndex + Len(str)
			
		Loop
	End If
End Function